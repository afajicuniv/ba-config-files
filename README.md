# ba-config-files



# INFO
This project contains the configuration files used to set up DevOps pipelines in GitHub Actions and GitLab Auto DevOps for my bachelor thesis.

GitHub Actions configuration file: github-actions-ci.yml
GitLab Auto DevOps configuration file: gitlab-ci.yml

# AUTHOR
Ahmedin Fajic


